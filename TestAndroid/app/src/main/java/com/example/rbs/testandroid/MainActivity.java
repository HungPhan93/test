package com.example.rbs.testandroid;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.graphics.Paint;
import android.os.Bundle;
import android.app.Activity;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        RelativeLayout rl = (RelativeLayout) findViewById(R.id.rl);
        final TextView tv = (TextView) findViewById(R.id.tv);
        Button btn = (Button) findViewById(R.id.btn);

        // Set a click listener for Button widget
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                /*
                    public int getPaintFlags ()
                        Returns
                        the flags on the Paint being used to display the text.

                    setPaintFlags(int flags)
                        Sets flags on the Paint being used to display the text and
                        reflows the text if they are different from the old flags.

                    STRIKE_THRU_TEXT_FLAG
                     Paint flag that applies a strike-through decoration to drawn text.
                 */

                // Set TextView text strike through
                tv.setPaintFlags(tv.getPaintFlags()| Paint.STRIKE_THRU_TEXT_FLAG);
            }
        });

    }
}
